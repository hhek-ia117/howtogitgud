# HowToGitGud
oder: wie man Git benutzt

**von Leon Grünewald und Jean Luc Nürrenberg**

------

## Was ist git?
Git ist eine freie Quellcodeverwaltungssoftware, entwickelt von Linux Torvalds

## Wie wird Git benutzt?

**Git Hilfe**
```
$ man git
```
oder
```
$ git <befehl> --help
```

**Benutzername und E-Mail in git setzen**
```
$ git config --global user.name "Max Mustermann"
$ git config --global user.email "max.mustermann@hhekann-nix.de"
```

**Ein git-Repository im aktuellen Verzeichnis initialisieren**
```
$ git init
```

**Den Status des git-Repositories anzeigen lassen**
```
$ git status
```

**Dateien in deinen Staging bereich hinzufügen**
```
$ git add /pfad/zu/der/datei.txt
```

**Ungesicherte Änderungen anzeigen**
```
$ git diff
```

**Staging bereich committen**
```
$ git commit -m "[Beschreiben was man geändert hat]"
```

**Alle Dateien stagen und committen**
```
$ git commit -a -m "[Beschreiben was man geändert hat]"
```

**Lokales Repository mit einem entfernten Repository verbinden**
```
$ git remote add [Remote-Name] [URL]
```

**Lokale Änderungen auf das entfernte Repository übertragen**
```
$ git push
```

**Änderungen vom entfernten Repository in das lokale Repository übertragen**
```
$ git pull
```

**Branch erstellen**
```
$ git checkout -b [Branch-Name]
```

**Branch wechseln**
```
$ git checkout [Branch-Name]
```

**Lokale Änderungen auf das entfernte Repository übertragen (erstes Mal)**
```
$ git push -u origin master
```

------

## Git Dienste
* [GitHub](https://github.com/)
* [GitLab](https://gitlab.com/)

## Git Dienste zum selber hosten:
* [GitTea](https://gitea.io/)
* [GitLab CE](https://about.gitlab.com/community/)

## Git GUI-Clients:
* [Git Extensions (Windows)](https://github.com/gitextensions/gitextensions)
* [Git Kraken (Windows, OS X, Linux)](https://www.gitkraken.com/)
* [Git Tower (Windows, OS X)](https://www.git-tower.com)

